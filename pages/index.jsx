import Home from 'components/Home'
import Team from 'components/Team'
import Gallery from 'components/Gallery'
import Highlight from 'components/Highlight'
import Contact from 'components/Contact'

import Link from 'next/link'


const Dashboard = () => (
    <>
        <Home />
        <Highlight />
        <Team />
        <Contact />
        
    </>
)
export default Dashboard