import styles from '../styles/upload.module.scss'
import DatePicker from "react-datepicker";
import Select from 'react-select'
import "react-datepicker/dist/react-datepicker.css";
import { useRef, useState } from 'react';
import Image from 'next/image'
import { v4 as uuidv4 } from 'uuid';
const testSuiteOptions = ["AF - API Penetration Testing", "AF - ASVS Level 2 Web Application"].map(i => ({ value: i, label: i }))
const scorings = ["Manual", "CVSS v3.1 Baseline", "CVSS v3.1 Baseline + Temporal"].map(i => ({ value: i, label: i }))

const upload = (props) => {
    const [image, setImage] = useState(null);
    const [createObjectURL, setCreateObjectURL] = useState(null);
    const form = useRef(null);
    // const [startDate, setStartDate] = useState(new Date());
    // const [endDate, setEndDate] = useState(new Date())
    // const [selectedtestSuite, handleTestSuite] = useState(null)
    // const [selectedScoring, handleScoring] = useState(null)
    // console.log(uuidv4())

    const createNewUser = (e) => {
        const uuid = uuidv4();
        const data = form.current
        const { name } = image;
        // console.log(image)
        console.log(createObjectURL)
        let obj = {
            user_id: uuid,
            first_name: data['fname'].value,
            last_name: data['lname'].value,
            aka: data['aname'].value,
            info: data['info'].value,
            profile_pic: uuid + "/" + name,
            // image: image,
        }

        const pictureUploadBody = new FormData();
        pictureUploadBody.append("files", image);


        fetch("/api/upload?id=" + uuid, {
            method: "POST",
            body: pictureUploadBody
        }).then(res => {
            console.log("pic upload successully " + res)
        })

        fetch("/api/table/user/create?user_id=" + uuid, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj)
        })
            // .then(res => res.json())
            .then(data => {
                // enter you logic when the fetch is successful
                console.log(data)
            })
            .catch(error => {
                // enter your logic for when there is an error (ex. error toast)
                console.log(error)
            })
        // console.log("body:" + body);
    }
    const uploadToClient = (event) => {
        if (event.target.files && event.target.files[0]) {
            const i = event.target.files[0];
            setImage(i);
            setCreateObjectURL(URL.createObjectURL(i));
        }
    };

    return (
        <section id="upload" className={styles.container}>
            <div className="content" >

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-6">
                            <form className="project-form" ref={form}>
                                <div className="mb-3">
                                    <label className="form-label">First Name *</label>
                                    <input type="text" name="fname" className="form-control" />
                                </div>
                                <div className="mb-3">
                                    <label className="form-label">Last Name *</label>
                                    <input type="text" name="lname" className="form-control" />
                                </div>
                                <div className="mb-3">
                                    <label className="form-label" >Alias Name</label>
                                    <input type="text" name="aname" className="form-control" />
                                </div>
                                <div className="mb-3">
                                    <label className="form-label">Information*</label>
                                    <input type="text" name="info" className="form-control" placeholder="E.g. application.com,192.168.0.1,/api/v1/endpoint,etc." />
                                </div>
                                {createObjectURL && <Image className="img-responsive img-circle" src={createObjectURL} width="100" height="100" />}
                                {/* <input type="file" name="myImage" onChange={uploadToClient} /> */}

                                <div className="input-group mb-3">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">Upload</span>
                                    </div>
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" onChange={uploadToClient} />
                                        <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
                                    </div>
                                </div>
                                {/* <div className="mb-3">
                                    <label className="form-label">Assign Test Suites</label>
                                    <Select
                                        placeholder="Select test type(s) to be perfomed.."
                                        value={selectedtestSuite}
                                        className="select-box"
                                        isClearable={false}
                                        maxMenuHeight={"22vh"}
                                        onChange={handleTestSuite}
                                        options={testSuiteOptions}
                                        isMulti
                                    />
                                </div> */}
                                {/* <div className="mb-3">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <label className="form-label">Birth</label>
                                            <div>
                                                <DatePicker
                                                    dateFormat="dd/MM/yyyy"
                                                selectsStart
                                                    selected={startDate}
                                                    onChange={(date) => setStartDate(date)}
                                                    className="form-control"
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <label className="form-label" >Died</label>
                                            <div>
                                                <DatePicker
                                                    dateFormat="dd/MM/yyyy"
                                                    selectsEnd
                                                    selected={endDate}
                                                    onChange={(date) => setEndDate(date)}
                                                    className="form-control"
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                                {/* <div className="mb-3">
                                    <label className="form-label" >Vulenerability Scoring System</label>
                                    <Select
                                        placeholder="Select test type(s) to be perfomed.."
                                        className="select-box"
                                        // defaultMenuIsOpen
                                        value={selectedScoring}
                                        isClearable={false}
                                        maxMenuHeight={"22vh"}
                                        onChange={handleScoring}
                                        options={scorings}
                                    />
                                </div> */}


                                <div onClick={(e) => createNewUser(e)} className="btn btn-sm btn-primary" >Create New User</div>
                            </form>
                        </div>
                        <div className="col-md-6">

                        </div>
                    </div>
                </div>

            </div>
        </section>
    )

}
export default upload;