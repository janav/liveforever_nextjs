
import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Loader = () => (
//     <div class="lds-heart">
//         <div></div>
//         </div>
<Box sx={{ display: 'flex' }}>
<CircularProgress />
</Box>
)

export default Loader;
