import { query } from "lib/db";
import Filter from 'bad-words'
import { memoriesOperationTypes } from "liveConstants";
const filter = new Filter()

export const getAllMemories = async (req, res) => {
    try {
        const results = await query(`SELECT * FROM memories`);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const getAllMemoriesByProfileId = async (req, res) => {
    let { profileId } = req.query
    try {
        if (!profileId) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `SELECT * FROM memories WHERE profileId = ?`;
        const results = await query(queryString, [profileId]);

        return res.json(results)
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const deleteMemoryById = async (req, res) => {
    let { id } = req.body
    try {
        if (!id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `UPDATE memories SET isDeleted = 1 WHERE id = ?`;
        const results = await query(queryString, [id]);
        return res.json(results)
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}


// UPDATE u158324213_life.memories
// SET profileId=NULL, image=NULL, description=NULL, memoryDate=NULL, city=NULL, state=NULL, country=NULL, isDeleted=NULL
// WHERE id=0;

export const updateMemoryById = async (req, res) => {
    let { id, image, description, memoryDate, city, state, country } = req.body
    try {
        if (!id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `UPDATE memories SET image = ?, description = ?, memoryDate = ?, city = ?, state = ?, country = ? WHERE id = ?`;
        const results = await query(queryString, [image, description, memoryDate, city, state, country, id]);
        return res.json(results)
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}


export const createMemory = async (req, res) => {
    let { profileId, image, description, memoryDate, city, state, country } = req.body

    if (description) {
        description = filter.clean(description);
    }
    try {
        const queryString = `
        INSERT INTO memories (profileId, image, description, memoryDate, city, state, country)
         VALUES (?, ?, ?, ?, ?, ?, ?)`;
        const results = await query(queryString, [profileId, image, description, memoryDate, city, state, country]);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const operation = async (req, res) => {
    const { operation } = req.query
    switch (operation) {
        case memoriesOperationTypes.getAllMemories: return getAllMemories(req, res);
        case memoriesOperationTypes.createMemory: return createMemory(req, res);
        case memoriesOperationTypes.getAllMemoriesByProfileId: return getAllMemoriesByProfileId(req, res);
        case memoriesOperationTypes.updateMemoryById: return updateMemoryById(req, res);
        case memoriesOperationTypes.deleteMemoryById: return deleteMemoryById(req, res);
        default: return res.status(400).json({ message: 'Invalid operation' });
    }
}


export default operation;