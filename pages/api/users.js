import { query } from "lib/db";
import Filter from 'bad-words'
import { userOperationTypes } from "liveConstants";
const filter = new Filter()


export const getAllUsers = async (req, res) => {
    try {
        const results = await query(`SELECT * FROM users`);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const checkAndCreateUser = async (req, res) => {
    let { email } = req.body;
    getByEmailFunction(email).then(async (result) => {
        if (result.length > 0) {
            return res.status(400).json({ message: "User already exists" })
        } else {
            let { name, profilePic } = req.body
            createUserFunction(name, email, profilePic).then(result => {
                return res.json(result)
            })
        }
    })
}


const createUser = async (req, res) => {
    let { name, email, profilePic } = req.body
    try {
        if (!name || !email || !profilePic) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        createUserFunction(name, email, profilePic).then(result => {
            return res.json(result)
        })
    }
    catch (e) {
        res.status(500).json({ message: e.message })

    }
}

const createUserFunction = (name, email, profilePic) => {
    const queryString = `INSERT INTO users ( name, email, profilePic) VALUES (?, ?, ?)`;
    return query(queryString, [name, email, profilePic]);
}


const deleteUserById = async (req, res) => {
    let { id } = req.body
    try {
        if (!id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `UPDATE users SET isDeleted = 1 WHERE id = ?`;
        const results = await query(queryString, [id]);
        return res.json(results)
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const updateUserById = async (req, res) => {
    let { id, role, name, email, profilePic } = req.body
    try {
        if (!role || !name || !email || !profilePic || !id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `UPDATE users SET role = ?, name = ?, email = ?, profilePic = ? WHERE id = ?`;
        const results = await query(queryString, [role, name, email, profilePic, id]);
        return res.json(results)
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}


const getByEmail = async (req, res) => {
    let { email } = req.body
    try {
        if (!email) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        getByEmailFunction(email).then(results => {
            return res.json(results)
        })
    }
    catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const getByEmailFunction = (email) => {
    const queryString = `SELECT * FROM users WHERE email = ?`;
    return query(queryString, [email]);
}

const operation = async (req, res) => {
    const { operation } = req.query
    switch (operation) {
        case userOperationTypes.getAllUsers: return getAllUsers(req, res);
        case userOperationTypes.createUser: return createUser(req, res);
        case userOperationTypes.getByEmail: return getByEmail(req, res);
        case userOperationTypes.updateUserById: return updateUserById(req, res);
        case userOperationTypes.checkAndCreateUser: return checkAndCreateUser(req, res);
        case userOperationTypes.deleteUserById: return deleteUserById(req, res);
        default: return res.status(400).json({ message: 'Invalid operation' });
    }
}

export default operation;