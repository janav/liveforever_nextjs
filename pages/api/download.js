import { env } from 'process';
import aws from 'aws-sdk';
import multerS3 from 'multer-s3';

// const s3 = new aws.S3({
//     accessKeyId: env.AWS_ACCESS_KEY_ID,
//     secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
//     region: env.AWS_REGION
// });

aws.config.update({
    accessKeyId: env.AWS_ACCESS_KEY_ID,
    secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
    region: env.AWS_REGION
  })
  
  const s3 = new aws.S3()

async function download(filename) {
    const { Body } = await s3.getObject({
        Key: filename,
        Bucket: 'pliveforever'
    }).promise()
    return Body
}


export default (req, res) => {

    return download("/pictures/1.jpg")
        .then(data => {
            console.log(data)
            res.send(data)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        });

};
