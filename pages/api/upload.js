import nextConnect from 'next-connect';
import multer from 'multer';
import fs from 'fs'
import { env } from 'process';

import aws from 'aws-sdk';
import multerS3 from 'multer-s3';


// const config = {
//   bucketName: 'myBucket',
//   dirName: 'media', /* optional */
//   region: 'eu-west-1',
//   accessKeyId: 'JAJHAFJFHJDFJSDHFSDHFJKDSF',
//   secretAccessKey: 'jhsdf99845fd98qwed42ebdyeqwd-3r98f373f=qwrq3rfr3rf',
//   s3Url: 'https:/your-custom-s3-url.com/', /* optional */
// }


// const upload = multer({
//   storage: multerS3({
//     s3: s3,
//     bucket: 'next-bucket',
//     acl: 'public-read',
//     metadata: function (req, file, cb) {
//       cb(null, { fieldName: file.fieldname });
//     },
//     key: function (req, file, cb) {
//       cb(null, Date.now().toString())
//     }
//   })
// });

const upload = multer({
  storage: multer.diskStorage({
    destination: ({ query }, f, cb) => {
      const path = `./${env.IMAGE_UPLOAD_DIR}/${query.id}`;
      console.log(path);
      fs.mkdirSync(path, { recursive: true })
      return cb(null, path);
    },
    filename: (req, file, cb) => {
      return cb(null, file.originalname)
    }
  }),
});


const apiRoute = nextConnect(
  {
    onError(error, req, res) {
      console.log("error");
      res.status(501).json({ error: `Sorry something Happened! ${error.message}` });
    },
    onNoMatch(req, res) {
      console.log("noMatcj");
      res.status(405).json({ error: `Method '${req.method}' Not Allowed` });
    },
  });

apiRoute.use(upload.array('files'));

apiRoute.post((req, res) => {
  res.status(200).json({ data: 'success' });
});

export default apiRoute;

export const config = {
  api: {
    bodyParser: false, // Disallow body parsing, consume as stream
  },
};