
import { query } from '../../../lib/db'
import Filter from 'bad-words'
const filter = new Filter()

import { tableConstants } from 'liveConstants'
const { c_user_id, c_first_name, c_last_name, c_info, c_aka, c_gender, c_birth_date, c_email
    , c_address, c_city, c_country, c_detailed_desc, c_details, c_header, c_image, c_profile_pic
    , c_state } = tableConstants

const createUserDetail = async (req, res) => {
  
    const { user_id, gender, birth_date, address, city, state, country, detailed_desc } = req.body
    try {

        if (!user_id) {
            return res
                .status(400)
                .json({ message: '`user_id` is required' })
        }

        const results = await query(
            `
            INSERT INTO users_details (`+ c_user_id + `,` + c_gender + `, `
            + c_birth_date + `, ` + c_address + `, ` + c_city + `,` + c_state + `, `
            + c_country + `, ` + c_detailed_desc + `)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            `,
            [filter.clean(user_id), filter.clean(gender),
                birth_date, filter.clean(address)
                , filter.clean(city), filter.clean(state)
                , filter.clean(country), filter.clean(detailed_desc)]
        )

        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}


const editUserDetailById = async (req, res) => {
    const { user_id, gender, birth_date, address, city, state, country, detailed_desc } = req.body
    try {
        if (!user_id) {
            return res
                .status(400)
                .json({ message: '`user_id` is required' })
        }
        const results = await query(
            `
            UPDATE users_details
            SET `+ c_gender + ` = ?, ` + c_birth_date + ` = ?, ` + c_address + ` = ?, ` + c_state + ` = ?,
            `+ c_state + ` = ?, ` + c_country + ` = ?, ` + c_detailed_desc + ` = ?
                       
            WHERE `+ c_user_id + ` = ?
            `,
            [filter.clean(gender), birth_date,
            filter.clean(address), filter.clean(city),
            filter.clean(state), filter.clean(country),
            filter.clean(detailed_desc), user_id]
        )
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const getuserDetails = async (req, res) => {

    try {
        const results = await query(`SELECT * FROM user_details`)
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
const getuserDetailById = async (req, res) => {
    const { user_id } = req.body;
    try {
        const results = await query(`SELECT * FROM user_details where ` + c_user_id + `= ?`, [user_id])
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}


export default getuserDetails
