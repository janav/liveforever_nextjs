
import Filter from 'bad-words'
import { query } from 'lib/db'
const filter = new Filter()


import { tableConstants } from 'liveConstants'
import { useRouter } from 'next/router'
const { c_user_id, c_created_by, c_roles, c_first_name, c_last_name, c_info, c_aka, c_gender, c_birth_date, c_email
    , c_address, c_city, c_country, c_detailed_desc, c_details, c_header, c_image, c_profile_pic
    , c_state } = tableConstants

const createUser = async (req, res) => {

    const { user_id, first_name, last_name, aka, info, profile_pic, roles, created_by } = req.body
    console.log(req.body);
    try {
        if (!first_name || !last_name) {
            return res
                .status(400)
                .json({ message: '`first_name`, `last_name` are all required' })
        }
        const queryString = `
        INSERT INTO user (`+ c_user_id + `,` + c_first_name + `,` + c_last_name + `, ` + c_aka + `, ` + c_info + `, ` + c_profile_pic + `, ` + c_roles + `, ` + c_created_by + `)
         VALUES (?, ?, ?, ?, ?, ?, ?, ?)
        `;
        console.log(queryString);
        const results = await query(queryString, [user_id, filter.clean(first_name), filter.clean(last_name),
            filter.clean(aka), filter.clean(info), profile_pic, roles, created_by]
        )
        console.log(results);
        return res.json(results)
    } catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message })
    }
}


const editUserById = async (req, res) => {
    const { user_id, first_name, last_name, aka, info } = req.body

    try {
        if (!user_id || !first_name || !last_name) {
            return res
                .status(400)
                .json({ message: '`id`, `first_name`, `last_name` are all required' })
        }
        const results = await query(
            `
            UPDATE user
            SET `+ c_first_name + ` = ?, ` + c_last_name + ` = ?, ` + c_aka + ` = ?, ` + c_info + ` = ?
            WHERE `+ c_user_id + ` = ?
            `,
            [filter.clean(first_name), filter.clean(last_name),
            filter.clean(aka), filter.clean(info), user_id]
        )
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const getusers = async (req, res) => {

    try {
        const results = await query(`SELECT * FROM user`)
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const getuserById = async (req, res) => {
    const { user_id } = req.query;
    try {
        const results = await query(`SELECT * FROM user where ` + c_user_id + `= ?`, [user_id])
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const getuserByName = async (req, res) => {
    const { name } = req.body;
    try {
        const results = await query(`SELECT * FROM user where ` + c_first_name + `= ? or ` + c_last_name + `= ?`,
            [name])
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

const operation = async (req, res) => {
    const { operation } = req.query
    console.log(operation);
    switch (operation) {
        case 'create': return createUser(req, res);
        case 'edit': return editUserById(req, res);
        case 'get': return getuserById(req, res);
        case 'getByName': return getuserByName(req, res);
        case 'getAll': return getusers(req, res);
        default: return res.status(400).json({ message: 'Invalid operation' });
    }
}

export default operation
