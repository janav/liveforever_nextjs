import { profilesOperationTypes } from "liveConstants";
import Filter from 'bad-words'
import { query } from 'lib/db'
const filter = new Filter()


export const getAllProfiles = async (req, res) => {
    try {
        const results = await query(`SELECT * FROM profiles`);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const getCurrentDayBirthdayProfiles = async (req, res) => {
    try {
        const results = await query(`SELECT * FROM profiles WHERE dateBorn = CURDATE()`);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const getCurrentDayDefferedProfiles = async (req, res) => {
    try {
        const results = await query(`SELECT * FROM profiles WHERE dateDeffered = CURDATE()`);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const getProfilesByUserId = async (req, res) => {
    let { userId } = req.query
    try {
        if (!userId) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const results = await query(`SELECT * FROM profiles WHERE userId = ?`, [userId]);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

// UPDATE u158324213_life.profiles
// SET userId=NULL, firstName=NULL, lastName=NULL, aka=NULL, profilePic=NULL, profileCover=NULL, dateBorn=NULL, dateDeffered=NULL, city=NULL, state=NULL, country=NULL, description=NULL, love=0, flowers=0, hug=0, pray=0, isDeleted=0
// WHERE id=0;

export const updateProfileById = async (req, res) => {
    let { id, userId, firstName, lastName, aka, profilePic, profileCover, dateBorn, dateDeffered, city, state, country, description } = req.body;

    if (firstName) {
        firstName = filter.clean(firstName);
    }
    if (lastName) {
        lastName = filter.clean(lastName);
    }
    if (aka) {
        aka = filter.clean(aka);
    }
    if (description) {
        description = filter.clean(description);
    }
    if (!id || !firstName || !lastName) {
        return res
            .status(400)
            .json({ message: '`id`, `first_name`, `last_name` are all required' })
    }
    try {
        const queryString = `
        UPDATE profiles
        SET userId=?, firstName=?, lastName=?, aka=?, profilePic=?, profileCover=?, dateBorn=?, dateDeffered=?, city=?, state=?, country=?, description=?
        WHERE id=?`;
        const results = await query(queryString, [userId, firstName, lastName, aka, profilePic, profileCover, dateBorn, dateDeffered, city, state, country, description, id]);
        return res.json(results)

    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const deleteProfileById = async (req, res) => {
    let { id } = req.body;
    try {
        if (!id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `UPDATE profiles SET isDeleted = 1 WHERE id = ?`;
        const results = await query(queryString, [id]);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export const getAllProfilesWithFilters = async (req, res) => {
    const { name, country, state, city } = req.query;
    try {
        let queryString = `SELECT * FROM profiles WHERE 1=1`;
        if (name && name !== '') {
            queryString += ` AND firstName LIKE '%${name}%'`;
        }
        if (country && country !== '') {
            queryString += ` AND country LIKE '%${country}%'`;
        }
        if (state && state !== '') {
            queryString += ` AND state LIKE '%${state}%'`;
        }
        if (city && city !== '') {
            queryString += ` AND city LIKE '%${city}%'`;
        }
        const results = await query(queryString);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}


export const createProfile = async (req, res) => {

    let { userId, firstName, lastName, aka, profilePic, profileCover, dateBorn, dateDeffered, city, state, country, description } = req.body
    console.log(req.body);
    if (firstName) {
        firstName = filter.clean(firstName);
    }
    if (lastName) {
        lastName = filter.clean(lastName);
    }
    if (aka) {
        aka = filter.clean(aka);
    }
    if (description) {
        description = filter.clean(description);
    }
    try {
        if (!firstName || !lastName) {
            return res
                .status(400)
                .json({ message: '`first_name`, `last_name` are all required' })
        }
        const queryString = `
        INSERT INTO profiles (userId, firstName, lastName, aka, profilePic, profileCover, dateBorn, dateDeffered, city, state, country, description)
         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        `;
        console.log(queryString);
        const results = await query(queryString, [userId, firstName, lastName,
            aka, profilePic, profileCover, dateBorn, dateDeffered, city, state, country, description]
        )
        console.log(results);
        return res.json(results)
    } catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message })
    }
}


export const supportProfile = async (req, res) => {
    let { id, love, flowers, hug, pray } = req.body;
    try {
        if (!id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const queryString = `
        UPDATE profiles
        SET love=?, flowers=?, hug=?, pray=?
        WHERE id=?`;
        const results = await query(queryString, [love, flowers, hug, pray, id]);
        return res.json(results)
    } catch (e) {
    }
}

export const getProfileById = async(req, res)=> {
    let { id } = req.body;
    try {
        if (!id) {
            return res.status(400).json({ message: 'Invalid request' })
        }
        const startTime =new Date().getTime();
        const queryString = `SELECT * FROM profiles WHERE id = ?`;
        const profile = await query(queryString, [id]);
          
        const memoryQuery = `SELECT * FROM memories WHERE profileId = ?`;
        const memories = await query(memoryQuery, [id]);
        const endTime = new Date().getTime();
        console.log(`time taken ${(endTime - startTime)/1000} seconds`);  
        const startTime1 =new Date().getTime();
        const joinQuery = `select * from profiles p  inner join memories m on p.id =m.profileId where p.id = ?`;
        const tags = await query(joinQuery, [id]);
        const endTime1 = new Date().getTime();
        console.log(`time taken ${(endTime1 - startTime1)/1000} seconds`);  
        // return res.json({profile, memories, tags})
        // console.log(memories);
        const results = {
            profile: profile[0],
            memories: memories
        }
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })

    }
}


const operation = async (req, res) => {
    const { operation } = req.query
    switch (operation) {
        case profilesOperationTypes.getAllProfiles: return getAllProfiles(req, res);
        case profilesOperationTypes.createProfile: return createProfile(req, res);
        case profilesOperationTypes.getAllProfilesWithFilters: return createProfile(req, res);
        case profilesOperationTypes.getProfilesByUserId: return getProfilesByUserId(req, res);
        case profilesOperationTypes.updateProfileById: return updateProfileById(req, res);
        case profilesOperationTypes.deleteProfileById: return deleteProfileById(req, res);
        case profilesOperationTypes.getProfileById: return getProfileById(req, res);
        case profilesOperationTypes.getCurrentDayBirthdayProfiles: return getCurrentDayBirthdayProfiles(req, res);
        case profilesOperationTypes.getCurrentDayDefferedProfiles: return getCurrentDayDefferedProfiles(req, res);
        case profilesOperationTypes.supportProfile: return supportProfile(req, res);
        default: return res.status(400).json({ message: 'Invalid operation' });
    }
}


export default operation;