import Filter from 'bad-words'
import { query } from 'lib/db'
const filter = new Filter()
import { tableConstants } from 'liveConstants'
const { c_user_id, c_created_by, c_roles, c_first_name, c_last_name, c_info, c_aka, c_gender, c_birth_date, c_email
    , c_address, c_city, c_country, c_detailed_desc, c_details, c_header, c_image, c_profile_pic
    , c_state } = tableConstants


export const getProfiles = async (req, res) => {
    const { user_id } = req.query;
    try {
        const results = await query(`SELECT * FROM user`);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}


export const getProfilesWithFilters = async (req, res) => {
    const { name, country, state, city } = req.query;

    try {

        let queryString = `SELECT * FROM user WHERE 1=1`;
        if (name && name !== '') {
            queryString += ` AND ` + c_first_name + ` LIKE '%${name}%'`;
        }
        if (country && country !== '') {
            queryString += ` AND ` + c_country + ` LIKE '%${country}%'`;
        }
        if (state && state !== '') {
            queryString += ` AND ` + c_state + ` LIKE '%${state}%'`;
        }
        if (city && city !== '') {
            queryString += ` AND ` + c_city + ` LIKE '%${city}%'`;
        }
        const results = await query(queryString);
        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export default getProfiles;