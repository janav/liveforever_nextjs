import Highlight from 'components/Highlight'
import UserHome from 'components/UserHome';
import { useSession } from 'next-auth/react';

const profile = () => {

    const { data: session } = useSession()
  
    return <>
        <UserHome />
    </>
}
export default profile;