import Head from "next/head";
import { useEffect } from "react"
import Layout from "components/Layout"

import 'styles/common.scss';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { AppWrapper } from "context/AppContext";
import { SessionProvider } from "next-auth/react";
import 'styles/globals.scss'; // imports tailwind

// import { SessionProvider } from "next-auth/react"


const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wowjs') : null

const App = ({ Component, pageProps: { session, ...pageProps } }) => {
  useEffect(() => {
    import('modernizr').then(() => { });
    new WOW.WOW().init()
    return {}
  }, [])
  return (
    <>
        <SessionProvider session={session}>
      <AppWrapper>
        <Head>
          <title>Live For Ever</title>
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AppWrapper>
      </SessionProvider>
    </>
  )
}

export default App