import Gallery from 'components/Gallery'
import { useRouter } from 'next/router'

const Post = () => {
  const router = useRouter()
  const { pid } = router.query
  console.log(pid)
  return <>
    <Gallery id={pid}/>
  </>
}

export default Post