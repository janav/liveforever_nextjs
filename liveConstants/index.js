export const profilesOperationTypes = {
    getAllProfiles: "getAllProfiles",
    createProfile: "createProfile",
    getAllProfilesWithFilters: "getAllProfilesWithFilters",
    updateProfileById: "updateProfileById",
    deleteProfileById: "deleteProfileById",
    getCurrentDayBirthdayProfiles: "getCurrentDayBirthdayProfiles",
    getCurrentDayDefferedProfiles: "getCurrentDayDefferedProfiles",
    getProfileById: "getProfileById",
    supportProfile: "supportProfile",
    getProfilesByUserId: "getProfilesByUserId",
    getProfilesById: "getProfilesById",
}

export const memoriesOperationTypes = {
    getAllMemories: "getAllMemories",
    getAllMemoriesByProfileId: "getAllMemoriesByProfileId",
    updateMemoryById: "updateMemoryById",
    deleteMemoryById: "deleteMemoryById",
    createMemory: "createMemory",
}

export const userOperationTypes = {
    getAllUsers: "getAllUsers",
    getByEmail: "getByEmail",
    createUser: "createUser",
    updateUserById: "updateUserById",
    deleteUserById: "deleteUserById",
    checkAndCreateUser: "checkAndCreateUser",
}

export const tableConstants = {
    c_user_id: 'user_id',
    c_first_name: 'first_name',
    c_last_name: 'last_name',
    c_profile_pic:'profile_pic',
    c_roles:'roles',
    c_created_by:'created_by',
    c_aka: 'aka',
    c_info: 'info',
    c_email: 'email',
    c_profile_pic: 'profile_pic',
    c_gender: 'gender',
    c_birth_date: 'birth_date',
    c_address: 'address',
    c_city: 'city',
    c_state: 'state',
    c_country: 'country',
    c_detailed_desc: 'detailed_desc',
    c_image: 'image',
    c_header: 'header',
    c_details: 'details'
}