const withModernizr = require("next-plugin-modernizr");
// const withImages = require('next-images')
module.exports = withModernizr({
    reactStrictMode: true,
    eslint: {
        ignoreDuringBuilds: true,
    },
    images: {
        domains: ['pliveforever.s3.amazonaws.com', 'example2.com','lh3.googleusercontent.com'],
      },
});

// module.exports = withImages({
//     images: {
//         domains: ['pliveforever.s3.amazonaws.com']
//     },
//     webpack(config, options) {
//         return config;
//     }
// });