import { Button } from "@material-ui/core";
import Highlight from "components/Highlight";
import Team from "components/Team";
import { useEffect, useState } from "react";

import Link from 'next/link'

const initial = [
    {
        "name": "Name 1",
        "desc": "Aliquam ac justo est. Praesent feugiat cursus est.",
        "image": "/images/1.jpg"
    },
    {
        "name": "Avici",
        "desc": "Swedish DJ, remixer, record producer, musician and songwriter",
        "image": "/images/A._P._J._Abdul_Kalam.jpg"
    },
    {
        "name": "Name 3",
        "desc": "Maecenas sed diam eget risus varius blandit sit non.",
        "image": "/images/2.jpg"
    },
    {
        "name": "Name 4",
        "desc": "Phasellus nec ante in nunc molestie tincidunt ut eu diam.",
        "image": "/images/3.jpg"
    },
    {
        "name": "Sandy, Director",
        "desc": "Curabitur vulputate euismod neque et tincidunt.",
        "image": "/images/team-img4.jpg"
    },
    {
        "name": "Jack, Founder",
        "desc": "Maecenas sed diam eget risus varius blandit sit non.",
        "image": "/images/team-img2.jpg"
    },
    {
        "name": "Linda, Manager",
        "desc": "Phasellus nec ante in nunc molestie tincidunt ut eu diam.",
        "image": "/images/team-img3.jpg"
    },
    {
        "name": "Sandy, Director",
        "desc": "Curabitur vulputate euismod neque et tincidunt.",
        "image": "/images/team-img4.jpg"
    },
    {
        "name": "Jack, Founder",
        "desc": "Maecenas sed diam eget risus varius blandit sit non.",
        "image": "/images/team-img2.jpg"
    },
    {
        "name": "Linda, Manager",
        "desc": "Phasellus nec ante in nunc molestie tincidunt ut eu diam.",
        "image": "/images/team-img3.jpg"
    },
    {
        "name": "Sandy, Director",
        "desc": "Curabitur vulputate euismod neque et tincidunt.",
        "image": "/images/team-img4.jpg"
    }
]


const Card = ({ item, index }) => {
    console.log(item);
    return (
        <div className="bg-[#181918] m-4 flex flex-1
      2xl:min-w-[450px]
      2xl:max-w-[500px]
      sm:min-w-[270px]
      sm:max-w-[300px]
      min-w-full
      flex-col p-3 rounded-md hover:shadow-2xl"
        >
            <div className="flex flex-col items-center w-full mt-3">


                <img
                    src={item.profileCover}
                    alt="nature"
                    className="w-full h-64 2xl:h-96 rounded-md shadow-lg object-cover"
                />
                <h2>{item.firstName},{item.lastName}</h2>
                <p> {item.description} </p>
                <div className="p-3 px-5 w-max-mt-5 shadow-2xl">
                    <p className="text-[#37c7da] font-bold ">
                        <Link href={`/user/${item.id}`} class="text-[#37c7da]"> EDIT </Link>

                    </p>
                </div>
            </div>
        </div >
    )
}
const UserHome = () => {

    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    const fetchData = async () => {
        const response = await fetch(`/api/profiles?operation=getAllProfiles`)
        const data = await response.json()
        setData(data);
        setIsLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <section className="pt-12">



            {
                isLoading ? (
                    <div className="flex justify-center">
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                ) : (
                    <div class="mx-auto w-full max-w-1xl px-005 sm:px-010">
                        <div>User Home</div>

                        <div className="flex flex-wrap justify-center items-center mt-10">
                            {
                                _.map(data, (item, index) => {
                                    return (
                                        index ? <Card index={index} item={item} /> : <></>
                                    )
                                })
                            }
                        </div>
                    </div>
                )
            }



        </section >
    );
}

export default UserHome;