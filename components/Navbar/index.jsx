import { useState } from 'react'
import { signin, signIn, signOut, useSession } from 'next-auth/react'

import styles from './navbar.module.scss'

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';

import Link from 'next/link'
import { Dialog, DialogTitle, Grid, List, ListItemAvatar } from '@material-ui/core';

import SignInSide from 'pages/SignInSide';
import { useAppContext } from 'context/AppContext';


import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import PersonIcon from '@mui/icons-material/Person';
import AddIcon from '@mui/icons-material/Add';
import { blue } from '@mui/material/colors';
import UserForm from 'components/multi-form/UserForm';
// import upload from 'pages/upload';

const pages = ['Products', 'Pricing', 'Blog'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const SimpleDialog = (props) => {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return <>
    <Dialog onClose={handleClose} open={open}>
      {/* <upload/> */}
      <Grid container >
        <UserForm />

      </Grid>
      {/* <DialogTitle>Set backup account</DialogTitle>
    <List sx={{ pt: 0 }}>
      {/* {emails.map((email) => (
        <ListItem button onClick={() => handleListItemClick(email)} key={email}>
          <ListItemAvatar>
            <Avatar sx={{ bgcolor: blue[100], color: blue[600] }}>
              <PersonIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={email} />
        </ListItem>
      ))} */}
      {/* <ListItem autoFocus button onClick={() => handleListItemClick('addAccount')}>
        <ListItemAvatar>
          <Avatar>
            <AddIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Add account" />
      </ListItem>
    </List> */}
    </Dialog>
  </>
}

const Navbar = () => {

  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);


  const [modal, setModal] = useState(false);


  const { state, dispatch } = useAppContext();


  React.useEffect(() => {
    return dispatch({
      ...state,
      modal: false
    });
  }, [])


  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const [open, setOpen] = useState(false);
  const handleUpload = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    // setSelectedValue(value);
  };


  const [toggle, setToggle] = useState(false)
  // const [session, loading] = useSession()
  const { data: session } = useSession()
  return (
    <div className="navbar navbar-default navbar-fixed-top" role="navigation">
      {/* <Modal open={this.state.showModal}>...</Modal> */}

      <Dialog
        open={modal}
        fullWidth
      // maxWidth='sm'
      >
        {modal &&
          <div>
            {/* <button>Sign in With Google</button> */}
            {/* <Button className="smoothScroll btn btn-success btn-lg wow fadeInUp" onClick={() => signIn("google")}>Sign in With Google</Button>
            <br/>
            <Button onClick={() => setModal(false)}>Close</Button> */}
            <SignInSide modal={modal} setModal={setModal} />

          </div>}
      </Dialog>
      <div className="container">
        <div className="navbar-header">
          <button className="navbar-toggle" onClick={() => setToggle(!toggle)}>
            <i className={!toggle ? "fa fa-bars" : "fa fa-close"} style={{ color: "#fff", fontSize: "20px" }} />
          </button>
          <div className="wow backInDown" data-wow-delay="0.4s">
            <a href="/" className="navbar-brand  smoothScroll" ><em style={{ color: '#e44c65' }}>L</em>ive <em style={{ color: '#e44c65' }}>F</em>or <em style={{ color: '#e44c65' }}>E</em>ver</a>
          </div>

        </div>
        <div className={toggle ? "navbar-collapse" : "collapse navbar-collapse"}>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <Link clasName="nav-button" href="/" className="smoothScroll">Home</Link>
            </li>
            <li>
              <Link href="/#about" className="smoothScroll">Highlights</Link>
            </li>
            <li>
              <a href="/#gallery" className="smoothScroll"><span>Gallery</span></a>
            </li>
            <li>
              <Link href="/#contact" className="smoothScroll">Contact</Link>
            </li>
            <li>
              {!session && <>

                <a className="smoothScroll"
                  href={`/api/auth/signin`}
                  // className={styles.buttonPrimary}
                  onClick={(e) => {
                    e.preventDefault()
                    // signIn();
                    setModal(true);
                  }}
                >
                  <span>
                    Sign in
                  </span>
                </a>
              </>}
              {session && <>
                {session.user.image &&
                  <span>
                    <Tooltip title="Profile">
                      <IconButton onClick={handleOpenUserMenu} >
                        <Avatar alt={session.user.name} src={session.user.image} />
                      </IconButton>
                    </Tooltip>
                    <Menu
                      sx={{ mt: '45px' }}
                      id="menu-appbar"
                      anchorEl={anchorElUser}
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                      }}
                      keepMounted
                      transformOrigin={{
                        // vertical: 'top',
                        horizontal: 'right',
                      }}
                      open={Boolean(anchorElUser)}
                      onClose={handleCloseUserMenu}
                    >
                      {/* {settings.map((setting) => ( */}
                      {/* <MenuItem key={setting} onClick={handleCloseNavMenu}> */}
                      <MenuItem onClick={handleCloseNavMenu} >
                        {/* <Typography textAlign="center">{setting}</Typography> */}
                        <Button href={`/upload`}>Upload</Button>
                        <Button onClick={handleUpload}>Upload</Button>
                        {/* <Link href={`/form`}>Upload</Link> */}
                      </MenuItem>
                      {/* ))} */}
                    </Menu>
                    <SimpleDialog
                      // selectedValue={selectedValue}
                      open={open}
                      onClose={handleClose}
                    />
                  </span>
                }
                <a
                  href={`/api/auth/signout`}
                  className={styles.button}
                  onClick={(e) => {
                    e.preventDefault()
                    signOut()
                  }}
                >
                  <span>
                    Sign out
                  </span>
                </a>

              </>}

            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Navbar;