import React from "react";

import Navbar from "../Navbar";
import Footer from "../Footer";
import { AppBar } from "@mui/material";
import ResponsiveAppBar from "../Navbar"; 

const Layout = ({ children }) => (
  <>
    <Navbar />
    {children}
    <Footer />
  </>
);

export default Layout;