import Slider from 'react-slick'
import _ from 'lodash'
import Image from 'next/image'
import { style1, style2 } from './style.module.scss'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import Loader from 'pages/Loader'
const initial = [
    {
        firstName: "Jery",
        description: "Quisque tempor bibendum dolor at volutpat. Suspendisse venenatis quam sed libero euismod feugiat. In cursus nisi vitae lectus facilisis mollis. Nullam scelerisque, quam nec iaculis vulputate.",
        profilePic: "https://pliveforever.s3.amazonaws.com/pictures/1.jpg"
    },
    {
        firstName: "Tom",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus ab dolores, voluptates eveniet, a fuga inventore, excepturi eum non deserunt modi. Veniam alias voluptatem ad suscipit deserunt tempora quisquam velit fuga blanditiis id ullam magnam repellat nesciunt temporibus quidem mollitia inventore nostrum, quibusdam quis rerum, iste minus veritatis ipsa. Cumque voluptatum repellendus sed voluptates, a libero, alias enim laboriosam id tempore dolorem delectus eius. Odio soluta repellendus tempore magni minima? Assumenda repudiandae pariatur tenetur cupiditate quia dolorem? A, obcaecati perspiciatis fugiat, provident expedita quaerat quam minima nostrum ullam cupiditate possimus rerum nobis magnam sunt labore unde quidem numquam! Reiciendis, quisquam?",
        profilePic: "https://pliveforever.s3.amazonaws.com/pictures/1.jpg"
    },
    {
        firstName: "Helenski",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta, neque!",
        profilePic: "https://pliveforever.s3.amazonaws.com/pictures/1.jpg"
    },
    {
        firstName: "Tokyo",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque voluptate culpa ex deleniti similique, magni quod placeat est molestias neque a quasi dolorem. Explicabo, consequuntur rerum. Saepe cumque dolores laborum?",
        profilePic: "https://pliveforever.s3.amazonaws.com/pictures/1.jpg"
    }
]
const settings = {
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 2000,
    // fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    pauseOnHover: false
}
const Highlight = () => {

    const [data, setData] = useState(null);

    const fetchData = async () => {
        const response = await fetch(`/api/profiles?operation=getAllProfiles`)
        const resData = await response.json()
        setData(resData);
    }

    useEffect(() => {
        fetchData();
    }, []);


    const getData = (data) => _.map(data, ({ id, firstName, lastName, description, profilePic, title, desc, image }, key) => <div key={key}>
        <div className="container">
            <div className="row">
                <div className="col-md-9 col-sm-8 wow fadeInUp" data-wow-delay="0.2s">
                    <div className="about-thumb">
                        <h1>{firstName}</h1>
                        <p>
                            {
                                description.slice(0, 200)
                            }
                        </p>
                        <Link href={"/user/" + id}>
                            <a>Read More</a>
                        </Link>
                    </div>
                </div>
                <div className={"col-md-3 col-sm-4 wow fadeInUp about-img"}>
                    <img src={profilePic} className="img-responsive img-circle" />
                </div>
            </div>
        </div>
    </div>
    )
    return (<section id="about">
        {/* <div className="container">
            <div className="row">
                <div className="col-md-9 col-sm-8 wow fadeInUp" data-wow-delay="0.2s">
                    <div className="about-thumb">
                        <h1>Background</h1>
                        <p>
                            Quisque tempor bibendum dolor at volutpat. Suspendisse venenatis
                            quam sed libero euismod feugiat. In cursus nisi vitae lectus
                            facilisis mollis. Nullam scelerisque, quam nec iaculis
                            vulputate.
                        </p>
                    </div>
                </div>
                <div className="col-md-3 col-sm-4 wow fadeInUp about-img" data-wow-delay="0.6s"  >
                    <img
                        src="images/about-img.jpg"
                        className="img-responsive img-circle"
                        alt="About"
                    />
                </div>
            </div>
        </div> */}
        <Slider {...settings} >{
            data ? getData(data) : <Loader />
        }
        </Slider>
    </section>)
}

export default Highlight