import SlideShow from 'react-background-slideshow'
import Image from 'next/image'
import _ from 'lodash'
// import { home } from './style.module.scss'
const images = ['/images/slide-img1.jpg', '/images/slide-img2.jpg', '/images/slide-img3.jpg']
const Home = () => {
    return (
        <section id="home">
            <div className="top"> </div>
            <div className="overlay"></div>
            <div className="container">
                <div className="row">
                    <div className="col-md-offset-1 col-md-10 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
                        <h1 className="wow fadeInUp" data-wow-delay="0.6s">
                            Live For Ever
                        </h1>
                        <p className="wow flipInX" data-wow-delay="0.9s">
                            Death Exists only when no one remembers 
                            <a rel="nofollow"> YOU</a>.
                        </p>
                        <a href="#about" className="smoothScroll btn btn-success btn-lg wow fadeInUp" data-wow-delay="1.2s">Learn more</a
                        >
                    </div>
                </div>
                </div>
        </section >
    )
}
export default Home;