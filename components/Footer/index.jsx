import React from 'react'
import _ from 'lodash'

const social = ['fa fa-facebook', 'fa fa-twitter', 'fa fa-linkedin', 'fa fa-instagram', 'fa fa-google-plus']

const Footer = () => {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-sm-12">
                        <ul className="social-icon">
                            {_.map(social, (name, i) => <li key={i}>
                                <a className={name + " wow fadeInUp"} data-wow-delay="0.2s"></a>
                            </li>)}
                        </ul>

                        <p className="wow fadeInUp" data-wow-delay="1s">
                            Copyright &copy; 2021 Live Forever
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}
export default Footer;