import Slider from 'react-slick'
import { team } from './style.module.scss'
import _ from 'lodash'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import { useEffect } from 'react'
import Loader from 'pages/Loader'
const initial = [
  {
    "name": "Name 1",
    "desc": "Aliquam ac justo est. Praesent feugiat cursus est.",
    "image": "/images/1.jpg"
  },
  {
    "name": "Avici",
    "desc": "Swedish DJ, remixer, record producer, musician and songwriter",
    "image": "/images/A._P._J._Abdul_Kalam.jpg"
  },
  {
    "name": "Name 3",
    "desc": "Maecenas sed diam eget risus varius blandit sit non.",
    "image": "/images/2.jpg"
  },
  {
    "name": "Name 4",
    "desc": "Phasellus nec ante in nunc molestie tincidunt ut eu diam.",
    "image": "/images/3.jpg"
  },
  {
    "name": "Sandy, Director",
    "desc": "Curabitur vulputate euismod neque et tincidunt.",
    "image": "/images/team-img4.jpg"
  },
  {
    "name": "Jack, Founder",
    "desc": "Maecenas sed diam eget risus varius blandit sit non.",
    "image": "/images/team-img2.jpg"
  },
  {
    "name": "Linda, Manager",
    "desc": "Phasellus nec ante in nunc molestie tincidunt ut eu diam.",
    "image": "/images/team-img3.jpg"
  },
  {
    "name": "Sandy, Director",
    "desc": "Curabitur vulputate euismod neque et tincidunt.",
    "image": "/images/team-img4.jpg"
  },
  {
    "name": "Jack, Founder",
    "desc": "Maecenas sed diam eget risus varius blandit sit non.",
    "image": "/images/team-img2.jpg"
  },
  {
    "name": "Linda, Manager",
    "desc": "Phasellus nec ante in nunc molestie tincidunt ut eu diam.",
    "image": "/images/team-img3.jpg"
  },
  {
    "name": "Sandy, Director",
    "desc": "Curabitur vulputate euismod neque et tincidunt.",
    "image": "/images/team-img4.jpg"
  }
]
const settings = {
  dots: false,
  arrows: false,
  infinite: true,
  speed: 500,
  autoplay: true,
  autoplaySpeed: 1300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
}
const Team = () => {
  const [data, setData] = useState(null);

  const fetchData = async () => {
    const response = await fetch(`/api/profiles?operation=getAllProfiles`)
    const data = await response.json()
    setData(data);
  }

  useEffect(() => {
    fetchData();
  }, []);


  const getData = (data) => _.map(data, ({ id, firstName, lastName, description, profileCover, title, desc, image, userId }, k) => <div key={k}>
    <div className="wow fadeInUp" style={{ paddingLeft: "20px" }} data-wow-delay="0.4s" >
      <div className={team}>
        <div>
          {console.log(profileCover)}
          {/* <Image src="`${profileCover}`" className="img-responsive img-circle" width="200" height="200" /> */}
          <Image src={profileCover} className="img-responsive img-circle" width="200" height="200" />
        </div>
        <h2>{firstName},{lastName}</h2>
        <p> {description}
          <Link href={"/user/" + id}>
            <a>Read More</a>
          </Link>
        </p>
      </div>
    </div></div>)

  return (
    <Slider {...settings} style={{ backgroundColor: "#ddd" }}>
      {data ? getData(data) : <Loader />}
    </Slider>
  );
}
export default Team
