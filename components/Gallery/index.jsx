

import { useAppContext } from 'context/AppContext';
import _ from 'lodash'
import Loader from 'pages/Loader';
import { useEffect, useState } from 'react';
// import { style1 } from './style.module.scss'

const initial = [
    {
        caption: "Sapien arcu",
        image: "https://pliveforever.s3.amazonaws.com/pictures/1.jpg",
        memoryDate: "1992-12-30T18:30:00.000Z",
        description: "Cum sociis natoque penatibus et magnis dis parturient montes."

    },
    {
        id: 10,
        profileId: 3,
        image: "https://lh3.googleusercontent.com/a-/AOh14Ggi9X6qi0YDssrHZkxDeacIl4uJvYg9yY82bYTuQRw=s96-c",
        description: "name2 is good",
        memoryDate: "1992-12-30T18:30:00.000Z",
        city: "Chennai",
        state: "TamilNadu",
        country: "India",
        isDeleted: 0
    },
    {
        caption: "Aliquam erat",
        image: "/images/gallery-img2.jpg",
        memoryDate: "1992-12-30T18:30:00.000Z",
        description: "Suspendisse venenatis quam sed libero euismod feugiat."
    },
    {
        caption: "Cras ante sem",
        image: "/images/gallery-img3.jpg",
        memoryDate: "1992-12-30T18:30:00.000Z",
        description: "vAenean urna massa, convallis vehicula velit et, dictum pellentesque nisi."
    },
    {
        caption: "Sed ornare",
        image: "/images/gallery-img4.jpg",
        memoryDate: "1992-12-30T18:30:00.000Z",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    }
]
const Gallery = ({ id }) => {

    console.log("id", id);
    const [profile, setProfile] = useState(null);
    const [memory, setMemory] = useState(null);
    const [data, setData] = useState(initial);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    const fetchData = async () => {
        fetch(`/api/profiles?operation=getProfileById`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id
            }),
        })
            .then(response => response.json())
            .then(json => {
                console.log("json", json);
                // const data = response.json();
                setProfile(json.profile);
                setMemory(json.memories);
                setData(json.memories);
                setIsLoading(false);
            })
            .catch(error => {
                setError(error);
                setIsLoading(false);
            })
    }
    useEffect(() => {
        fetchData();
    }, [id]);


    // const { state, dispatch } = useAppContext();

    // const { number } = state;

    // useEffect(() => {
    //     dispatch({ type: "add_number", value: 3 });
    // }, [])

    // console.log(number);

    return (
        <section id="gallery" className='pt-12'>
            {(!isLoading && profile) ? (
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card profile-card-2">
                                <div className="card-img-block">
                                    {/* <img className="img-fluid" src="/images/slide-img2.jpg" alt="Card image cap" /> */}
                                    <img className="img-fluid" src={profile.profileCover} alt="Card image cap" />
                                </div>
                                <div className="card-body">
                                    {/* <img src="/images/team-img1.jpg" alt="profile-image" className="profile" /> */}
                                    <img src={profile.profilePic} alt="profile-image" className="profile" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-offset-2 col-md-8 col-sm-12 wow fadeInUp" data-wow-delay="0.3s" >
                            <div className="section-title">
                                <h1>{profile.firstName + `,` + profile.lastName}</h1>
                                <p>
                                    {profile.city + `,` + profile.state + `,` + profile.country}
                                </p>
                                <p>
                                    {profile.description}
                                </p>
                            </div>
                        </div>

                        <ul className="grid1 cs-style-4 wow bounceIn">
                            {_.map(data, ({ caption, shortDesc, image ,description,memoryDate}, k) => <li key={k} className="col-md-6 col-sm-6">
                                <figure >
                                    <div><img src={image} alt="image 1" /></div>
                                    <figcaption>
                                        <h1>{caption}</h1>
                                        <div>
                                        <small>{description}</small>
                                        </div>
                                        <small>{new Date(memoryDate).toLocaleDateString()}</small>
                                        <a href="#">Read More</a>
                                    </figcaption>
                                </figure>
                            </li>
                            )}
                        </ul>
                    </div>
                </div>) : (<Loader />)
            }
        </section>
    )
}
export default Gallery